import { IBlockExpRecordNew, IKeyPair, ITransPayload } from './transactions'
import { IRecord } from './transactions'

export const handleIncomingData = data => {
  if (!data.unspentTransactions) {
    return []
  }

  return data.unspentTransactions.map(el => {
    const id: string = el.transactionId
    const amount: number = parseInt(el.value)
    return { id, amount } as IRecord
  })
}

export const handleGenKeyData = data => {
  const result: IKeyPair = {
    private: '',
    adress: ''
  }
  result.private = data.keyPriv.data
  result.adress = data.address.data
  return result
}

export const transformTransData = (data: ITransPayload) => {
  const outputs: any = []

  data.outgoing.forEach(el =>
    outputs.push({
      address: {
        type: 'SHA256',
        data: el.id
      },
      value: { value: el.amount.toString() },
      data: el.data
    })
  )

  const result = {
    transactionsIds: data.incoming,
    outputs,
    fees: { value: '0' },
    keyPriv: data.privateKey
  }
  return result
}

export const getTransactionTotal = transactions => {
  let total = 0

  transactions.forEach(trans => {
    let transTotal = 0

    trans.outputs.forEach(output => {
      transTotal += parseInt(output.value.value)
    })

    total += transTotal
  })

  return total
}

export const xfBlockData = data => {
  // const data = serverData.blocks[0]

  const date = data.header.timestamp.data
  const id = data.header.id.data
  const height = data.header.height
  const sender = data.header.author.rawData
  /**
   * Handle the case where data.transactions is undefined...
   */
  const transactions = data.transactions ? data.transactions.length : 0
  const total = data.transactions ? getTransactionTotal(data.transactions) : 0

  // // const total = getTotal(transactions)
  const result: IBlockExpRecordNew = {
    id,
    date,
    height,
    sender,
    transactions,
    total
  }
  return result
}

export const handleLastBlocksData = data => {
  const { blocks } = data
  return blocks.map(block => xfBlockData(block))
}
