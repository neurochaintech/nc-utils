import { IRecord } from './../lib/transactions.d'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import {
  GEN_KEY_ENDPOINT,
  genKeys,
  getIncTrans,
  getLastBlocks,
  IBlockExpRecordNew,
  IKeyPair,
  ITransPayload,
  LAST_BLOCKS_ENDPOINT,
  LIST_TRANS_ENDPOINT
} from './transactions'
import {
  getTransactionTotal,
  handleGenKeyData,
  handleIncomingData,
  handleLastBlocksData,
  transformTransData,
  xfBlockData
} from './transactionsFns'

describe('getIncTrans function', () => {
  test('Handle incoming transactions data should transform data correctly', () => {
    const mockIncoming = {
      unspentTransactions: [
        {
          transactionId: '/yYWmEyqfHRJIrrOrxcRL+TiDcIAhjr7XQQ+EhNPGsc=',
          value: '1000'
        },
        {
          transactionId: 'foo',
          value: '22'
        },
        {
          transactionId: 'bar',
          value: '1'
        }
      ]
    }

    const expected: IRecord[] = [
      {
        id: '/yYWmEyqfHRJIrrOrxcRL+TiDcIAhjr7XQQ+EhNPGsc=',
        amount: 1000
      },
      {
        id: 'foo',
        amount: 22
      },
      {
        id: 'bar',
        amount: 1
      }
    ]

    expect(handleIncomingData(mockIncoming)).toEqual(expected)
  })

  test('getIncTrans should return the right value if there are unspent transactions', () => {
    const mockIncoming = {
      unspentTransactions: [
        {
          transactionId: '/yYWmEyqfHRJIrrOrxcRL+TiDcIAhjr7XQQ+EhNPGsc=',
          value: '1000'
        },
        {
          transactionId: 'foo',
          value: '22'
        },
        {
          transactionId: 'bar',
          value: '1'
        }
      ]
    }

    const expected = [
      { amount: 1000, id: '/yYWmEyqfHRJIrrOrxcRL+TiDcIAhjr7XQQ+EhNPGsc=' },
      { amount: 22, id: 'foo' },
      { amount: 1, id: 'bar' }
    ]

    const mock = new MockAdapter(axios)
    mock.onGet(LIST_TRANS_ENDPOINT).reply(200, mockIncoming)
    return getIncTrans('foo').then(data => expect(data).toEqual(expected))
  })

  test('getIncTrans should return the right value if there are NO unspent transactions (server responds with empty object in that case...)', () => {
    const mockIncoming = {}

    const expected = []

    const mock = new MockAdapter(axios)
    mock.onGet(LIST_TRANS_ENDPOINT).reply(200, mockIncoming)
    return getIncTrans('foo').then(data => expect(data).toEqual(expected))
  })
})

describe('genKey function', () => {
  test('Handle incoming key data should tranform data correctly ', () => {
    const mockKey = {
      keyPriv: {
        type: 'ECP256K1',
        data:
          'MIIBGwIBADCB7AYHKoZIzj0CATCB4AIBATAsBgcqhkjOPQEBAiEA/////////////////////////////////////v///C8wRAQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHBEEEeb5mfvncu6xVoGKVzocLBwKb/NstzijZWfKBWxb4F5hIOtp3JqPEZV2k+/wOEQio/Re0SKaFVBmcR9CP+xDUuAIhAP////////////////////66rtzmr0igO7/SXozQNkFBAgEBBCcwJQIBAQQgQV5UUHQDEoD+GtkD4imguhfQ1565AxOxdSJts5E06+E='
      },
      keyPub: {
        type: 'ECP256K1',
        rawData:
          'MIIBMzCB7AYHKoZIzj0CATCB4AIBATAsBgcqhkjOPQEBAiEA/////////////////////////////////////v///C8wRAQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHBEEEeb5mfvncu6xVoGKVzocLBwKb/NstzijZWfKBWxb4F5hIOtp3JqPEZV2k+/wOEQio/Re0SKaFVBmcR9CP+xDUuAIhAP////////////////////66rtzmr0igO7/SXozQNkFBAgEBA0IABKFj4aG7lU7ZeLSY8r2y0e6bE6ehYM0SCCQB4xlZfvK83su9FNthlOuCWHcIt/0XWqIuUq+EPImWwswlTjxZRM8='
      },
      address: {
        type: 'SHA256',
        data: 'I0IAcMZv0OBtLF12FE0iIhhXN5usuzGIzXn03NCIWBU='
      }
    }
    const expected: IKeyPair = {
      private:
        'MIIBGwIBADCB7AYHKoZIzj0CATCB4AIBATAsBgcqhkjOPQEBAiEA/////////////////////////////////////v///C8wRAQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHBEEEeb5mfvncu6xVoGKVzocLBwKb/NstzijZWfKBWxb4F5hIOtp3JqPEZV2k+/wOEQio/Re0SKaFVBmcR9CP+xDUuAIhAP////////////////////66rtzmr0igO7/SXozQNkFBAgEBBCcwJQIBAQQgQV5UUHQDEoD+GtkD4imguhfQ1565AxOxdSJts5E06+E=',
      adress: 'I0IAcMZv0OBtLF12FE0iIhhXN5usuzGIzXn03NCIWBU='
    }

    expect(handleGenKeyData(mockKey)).toEqual(expected)
  })

  test('genKey should return the right value', () => {
    const mockKey = {
      keyPriv: {
        type: 'ECP256K1',
        data: 'foobar'
      },
      keyPub: {
        type: 'ECP256K1',
        rawData: 'strawberry'
      },
      address: {
        type: 'SHA256',
        data: 'simpson'
      }
    }
    const mock = new MockAdapter(axios)
    mock.onGet(GEN_KEY_ENDPOINT).reply(200, mockKey)

    return genKeys().then(keys => {
      expect(keys).toEqual({ adress: 'simpson', private: 'foobar' })
    })
  })
})

test('transformTransData function should return the right data', () => {
  const input: ITransPayload = {
    incoming: ['foo', 'bar', 'baz'],
    outgoing: [
      { id: 'cool', amount: 7, data: 'myData' },
      { id: 'bam', amount: 8 },
      { id: 'boom', amount: 9 }
    ],
    privateKey: 'myKey'
  }

  const expected = {
    transactionsIds: ['foo', 'bar', 'baz'],
    outputs: [
      {
        address: {
          type: 'SHA256',
          data: 'cool'
        },
        value: { value: '7' },
        data: 'myData'
      },
      {
        address: {
          type: 'SHA256',
          data: 'bam'
        },
        value: { value: '8' }
      },
      {
        address: {
          type: 'SHA256',
          data: 'boom'
        },
        value: { value: '9' }
      }
    ],
    fees: { value: '0' },
    keyPriv: 'myKey'
  }

  expect(transformTransData(input)).toEqual(expected)
})

describe('getLastBlocks function', () => {
  test('getLastBlocks should return the right value', () => {
    const mockData = {
      blocks: [
        {
          header: {
            id: {
              type: 'SHA256',
              data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
            },
            timestamp: { data: 1539641469 },
            previousBlockHash: { type: 'SHA256', data: '' },
            author: {
              type: 'ECP256K1',
              rawData: 'senderIdisthis'
            },
            height: 999
          },
          transactions: [
            {
              inputs: [
                { id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }
              ],
              outputs: [
                {
                  address: {
                    type: 'SHA256',
                    data: 'z5iMSrp79h/zueBTSzitqJW8rNXh4zEdYY96sl3tzkE='
                  },
                  value: { value: '2' },
                  data: 'dHJheCBraWxsZWQgbWU='
                },
                {
                  address: {
                    type: 'SHA256',
                    data: 'z5iMSrp79h/zzzzz='
                  },
                  value: { value: '4' },
                  data: 'dHJheCBraWxsZWQgbWU='
                }
              ],
              fees: { value: '0' },
              id: {
                type: 'SHA256',
                data: '+qO4Ja1GitcTlbWdgm+X+ko0cRLuM5wIHopM03uIkNE='
              },
              blockId: {
                type: 'SHA256',
                data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
              }
            },
            {
              inputs: [
                { id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }
              ],
              outputs: [
                {
                  address: {
                    type: 'SHA256',
                    data: '9NCTEMuu0heGrqNH+HB7iR8+ld6EQZnEWa8QcrcXJ/4='
                  },
                  value: { value: '11' },
                  data: 'd2hhdCBvbGl2aWVyID8='
                }
              ],
              fees: { value: '0' },
              id: {
                type: 'SHA256',
                data: '55my4o7v5qiG9R000eyCSHW+BjqqKtn3DIa0G4YINi4='
              },
              blockId: {
                type: 'SHA256',
                data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
              }
            }
          ]
        }
      ]
    }

    const result: IBlockExpRecordNew[] = [
      {
        id: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME=',
        date: 1539641469,
        height: 999,
        sender: 'senderIdisthis',
        transactions: 2,
        total: 17
      }
    ]
    const mock = new MockAdapter(axios)
    mock.onGet(LAST_BLOCKS_ENDPOINT).reply(200, mockData)

    return getLastBlocks().then(lastBLocks => {
      expect(lastBLocks).toEqual(result)
    })
  })

  test('xfBlockData should return the right value', () => {
    const input = {
      header: {
        id: {
          type: 'SHA256',
          data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
        },
        timestamp: { data: 1539641469 },
        previousBlockHash: { type: 'SHA256', data: '' },
        author: {
          type: 'ECP256K1',
          rawData: 'senderIdisthis'
        },
        height: 999
      },
      transactions: [
        {
          inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
          outputs: [
            {
              address: {
                type: 'SHA256',
                data: 'z5iMSrp79h/zueBTSzitqJW8rNXh4zEdYY96sl3tzkE='
              },
              value: { value: '2' },
              data: 'dHJheCBraWxsZWQgbWU='
            },
            {
              address: {
                type: 'SHA256',
                data: 'z5iMSrp79h/zzzzz='
              },
              value: { value: '4' },
              data: 'dHJheCBraWxsZWQgbWU='
            }
          ],
          fees: { value: '0' },
          id: {
            type: 'SHA256',
            data: '+qO4Ja1GitcTlbWdgm+X+ko0cRLuM5wIHopM03uIkNE='
          },
          blockId: {
            type: 'SHA256',
            data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
          }
        },
        {
          inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
          outputs: [
            {
              address: {
                type: 'SHA256',
                data: '9NCTEMuu0heGrqNH+HB7iR8+ld6EQZnEWa8QcrcXJ/4='
              },
              value: { value: '10' },
              data: 'd2hhdCBvbGl2aWVyID8='
            }
          ],
          fees: { value: '0' },
          id: {
            type: 'SHA256',
            data: '55my4o7v5qiG9R000eyCSHW+BjqqKtn3DIa0G4YINi4='
          },
          blockId: {
            type: 'SHA256',
            data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
          }
        }
      ]
    }

    const result: IBlockExpRecordNew = {
      id: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME=',
      date: 1539641469,
      height: 999,
      sender: 'senderIdisthis',
      transactions: 2,
      total: 16
    }

    expect(xfBlockData(input)).toEqual(result)
  })

  test('xfBlockData should handle the case where transactions are undefined', () => {
    const input = {
      header: {
        id: {
          type: 'SHA256',
          data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
        },
        timestamp: { data: 1539641469 },
        previousBlockHash: { type: 'SHA256', data: '' },
        author: {
          type: 'ECP256K1',
          rawData: 'senderIdisthis'
        },
        height: 999
      }
    }

    const result: IBlockExpRecordNew = {
      id: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME=',
      date: 1539641469,
      height: 999,
      sender: 'senderIdisthis',
      transactions: 0,
      total: 0
    }

    expect(xfBlockData(input)).toEqual(result)
  })

  test('getTransactionTotal', () => {
    const input = [
      {
        inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
        outputs: [
          {
            address: {
              type: 'SHA256',
              data: 'z5iMSrp79h/zueBTSzitqJW8rNXh4zEdYY96sl3tzkE='
            },
            value: { value: '2' },
            data: 'dHJheCBraWxsZWQgbWU='
          },
          {
            address: {
              type: 'SHA256',
              data: 'z5iMSrp79h/zzzzz='
            },
            value: { value: '4' },
            data: 'dHJheCBraWxsZWQgbWU='
          }
        ],
        fees: { value: '0' },
        id: {
          type: 'SHA256',
          data: '+qO4Ja1GitcTlbWdgm+X+ko0cRLuM5wIHopM03uIkNE='
        },
        blockId: {
          type: 'SHA256',
          data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
        }
      },
      {
        inputs: [{ id: { type: 'SHA256', data: '' }, outputId: 0, keyId: 0 }],
        outputs: [
          {
            address: {
              type: 'SHA256',
              data: '9NCTEMuu0heGrqNH+HB7iR8+ld6EQZnEWa8QcrcXJ/4='
            },
            value: { value: '10' },
            data: 'd2hhdCBvbGl2aWVyID8='
          }
        ],
        fees: { value: '0' },
        id: {
          type: 'SHA256',
          data: '55my4o7v5qiG9R000eyCSHW+BjqqKtn3DIa0G4YINi4='
        },
        blockId: {
          type: 'SHA256',
          data: 'V9koEBFGvXpfXekRzYk8nd3zrn6FY+7Q8xPMaPloVME='
        }
      }
    ]

    expect(getTransactionTotal(input)).toEqual(16)
  })
})
