import {
  handleGenKeyData,
  handleIncomingData,
  handleLastBlocksData,
  transformTransData
} from './transactionsFns'

import axios from 'axios'

export interface IRecord {
  id: string
  amount: number
  data?: string
}

/**
 * This is returned when you query a private key generation
 */
export interface IKeyPair {
  private: string
  adress: string
}

/**
 * Client Interface for transaction payload creation
 */
export interface ITransPayload {
  incoming: string[]
  outgoing: IRecord[]
  privateKey: string
}

let ROOT = ''
export const GEN_KEY_ENDPOINT = '/generate_keys'
export const LIST_TRANS_ENDPOINT = '/list_transactions'
export const PUBLISH_TRANS_ENDPOINT = '/publish_transaction'
export const LAST_BLOCKS_ENDPOINT = '/get_last_blocks'
export const TOTAL_TRANS_ENDPOINT = '/total_nb_transactions'
export const TOTAL_BLOCKS_ENDPOINT = '/total_nb_blocks'
export const GET_BLOCK_ENDPOINT = '/get_block'
export const GET_TRANSACTION_ENDPOINT = '/get_transaction'

const extractData = response => response.data

export const getIncTrans = (address: string) => {
  return axios
    .get(ROOT + LIST_TRANS_ENDPOINT, {
      params: {
        address: encodeURI(address)
      }
    })
    .then(extractData)
    .then(handleIncomingData)
}

/**
 * Execute Neurochain transaction
 */
export const execTrans = (trans: ITransPayload) => {
  const payload = transformTransData(trans)
  return axios.post(ROOT + PUBLISH_TRANS_ENDPOINT, payload)
}

/**
 * Generate neurochain keys
 */
export const genKeys: () => Promise<IKeyPair> = () => {
  return axios
    .get(ROOT + GEN_KEY_ENDPOINT)
    .then(extractData)
    .then(handleGenKeyData)
}

/**
 * Block Explorer things
 */
export interface IBlockExpRecordNew {
  id: string
  height: number
  date: number
  transactions: number
  total: number
  sender: string
}

export const getLastBlocks = (nbBlocks = 10) => {
  return axios
    .get(ROOT + LAST_BLOCKS_ENDPOINT, {
      params: {
        nb_blocks: nbBlocks
      }
    })
    .then(extractData)
    .then(handleLastBlocksData)
}

export const getTotalTrans = () => {
  return axios.get(ROOT + TOTAL_TRANS_ENDPOINT).then(extractData)
}

export const getTotalBlocks = () => {
  return axios.get(ROOT + TOTAL_BLOCKS_ENDPOINT).then(extractData)
}

export const getBlock = (height: number) => {
  return axios
    .get(ROOT + GET_BLOCK_ENDPOINT, {
      params: {
        height
      }
    })
    .then(extractData)
}

export const getTransaction = (id: string) => {
  return axios
    .get(ROOT + GET_TRANSACTION_ENDPOINT, {
      params: {
        transaction_id: encodeURI(id)
      }
    })
    .then(extractData)
}

export const setRoot = root => (ROOT = root)
