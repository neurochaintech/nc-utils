# Neurochain Utils

Functions to do calls to the neurochain BOT REST server.

The source code is written in typescript.

Use the commands below to build the library and to run it's test harness.

### Commands

Build project

```sh
yarn build
```

Build project in watch mode

```sh
yarn start
```

Run tests

```sh
yarn test
```

Run tests in watch mode

```sh
yarn test:watch
```

### How to use

Install libray in your project

```sh
yarn add nc-utils
```

Upgrade libray in your project

```sh
yarn upgrade nc-utils
```

Import a function of nc-utils in your app

```sh
import {getLastBlocks} from 'nc-utils'
```

### How to publish on NPM

1 If not already logged in, login to npm

```sh
npm login
```

2 bump version

```sh
npm version patch | minor | major
```

3 build library

```sh
yarn build
```

4 publish

```sh
npm publish
```

Note: don't forget to `sh yarn upgrade` in your apps that depend on nc-utils (duh)

### Minimal Docs

Below are the functions available in this library.

##### getLastBlocks

Gets the last blocks n blocks of Neurochain. Default value is 10.
Returns a promise.

##### getTotalTrans

Gets the total amount of transactions issued in Neurochain.
Returns a promise.

##### getTotalBlocks

Gets the total amount of blocks in Neurochain.
Returns a promise.

##### getBlock

Gets block by height.
Returns a promise.

##### getTransaction

Gets transaction by it's hash.
Returns a promise.

##### setRoot

Synchronous.

Sets the root of the url path for the library's api calls.

Default is just "/" which would assume you are runing the library on the same host as the BOT REST server.
